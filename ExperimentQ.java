import java.util.*;

public class ExperimentQ {
	public static void main (String[] args) {
		String num = args[0];
		int n = Integer.parseInt(num);
		String qindex = args[1];
		MyQueue queue = null;
		switch (qindex) {
		case "1": queue = new MyStack(Math.max(50, n/2));
		case "2": queue = new FourStacks(Math.max(50, n/4));
		case "3": queue = new MyLinkedList();
		case "4": queue = new Heap(Math.max(50, n/2));
		}
		int jobCount = 0;
		double totalDelay = 0.0;
		double p = 0.8;
		int time = 0;
		long startTime = System.currentTimeMillis();
		Random generator = new Random(100000000);
		
		for (time = 0; time < n; time++) {
			String flip;
			double priority;
			if (generator.nextDouble() < p) 
				flip = "heads";
			else flip = "tails";
			if (flip == "heads") {
				priority = generator.nextDouble();
				jobCount++;
				queue.add(new Job(time, priority));
			}
			else if (! queue.empty()) {
				Job nextJob = queue.pop();
				totalDelay += ((time - nextJob.creationTime) * nextJob.priority);
			}
			if (time == n/2) p = 0.2;
		}
		
		while (! queue.empty()) {
			Job nextJob = queue.pop();
			totalDelay += ((time - nextJob.creationTime) * nextJob.priority);
			time++;
		}
		System.out.println("Running time = " + (System.currentTimeMillis() - startTime) +
				" Quality = " + totalDelay/n);
	}
}
