import java.util.*;

public class TestQ {
	public static void main(String[] args) {
		System.out.println("J <priority>\t: Create a job with the specified priority."
							+ "The priority is a floating point number");
		System.out.println("E\t\t: Pop a job off the queue and execute it");
		System.out.println("F\t\t: Repeatedly pop jobs off the queue and execute them" 
							+ "until the queue is empty. Exit the program");
		
		int time = 0;
		int count = 0;
		MyStack s = new MyStack(50);
		FourStacks f = new FourStacks(50);
		MyLinkedList l = new MyLinkedList();
		Heap h = new Heap(50);
		double sSum = 0.0;
		double fsSum = 0.0;
		double lSum = 0.0;
		double hSum = 0.0;
		
		while (true) {
			Scanner sc = new Scanner(System.in);
			String input = sc.next();
			if (input.equals("J")) {
				String p = sc.next();
				double priority = Double.parseDouble(p);
				Job newJob = new Job(time, priority);
				String t = Integer.toString(time);
				newJob.name = "J" + t;
				s.add(newJob);
				f.add(newJob);
				l.add(newJob);
				h.add(newJob);
				System.out.println("Creating J" + time + " priority " + priority);
				time++;
				count++;
			}	// end J
			if (input.equals("E")) {
				Job sJob = s.pop();
				Job fJob = f.pop();
				Job lJob = l.pop();
				Job hJob = h.pop();

				System.out.println("Stack: Executing " + sJob.name + ". Delay cost " + 
						sJob.priority * (time-sJob.creationTime));
				sSum += sJob.priority * (time-sJob.creationTime);
				System.out.println("Four stacks: Executing " + fJob.name + ". Delay cost " +
						fJob.priority * (time-fJob.creationTime));
				fsSum += fJob.priority * (time-fJob.creationTime);
				System.out.println("Linked list: Executing " + lJob.name + ". Delay cost " +
						lJob.priority * (time-lJob.creationTime));
				lSum += lJob.priority * (time-lJob.creationTime);
				System.out.println("Heap: Executing " + hJob.name + ". Delay cost " +
						hJob.priority * (time-hJob.creationTime));
				hSum += hJob.priority * (time-hJob.creationTime);
				time++;
			}	// end E
			if (input.equals("F")) {
				while (! s.empty()) {
					Job sJob = s.pop();
					Job fJob = f.pop();
					Job lJob = l.pop();
					Job hJob = h.pop();
					System.out.println("Stack: Executing " + sJob.name + ". Delay cost " + 
							sJob.priority * (time-sJob.creationTime));
					sSum += sJob.priority * (time-sJob.creationTime);
					System.out.println("Four stacks: Executing " + fJob.name + ". Delay cost " +
							fJob.priority * (time-fJob.creationTime));
					fsSum += fJob.priority * (time-fJob.creationTime);
					System.out.println("Linked list: Executing " + lJob.name + ". Delay cost " +
							lJob.priority * (time-lJob.creationTime));
					lSum += lJob.priority * (time-lJob.creationTime);
					System.out.println("Heap: Executing " + hJob.name + ". Delay cost " +
							hJob.priority * (time-hJob.creationTime));
					hSum += hJob.priority * (time-hJob.creationTime);
					time++;
				}
				System.out.println("Quality:\nStack: " +  (sSum/count) + 
						"\nFour Stacks: " + (fsSum/count) + 
						"\nLinked list: " + (lSum/count) +
						"\nHeap: " + (hSum/count));
				sc.close();
				break;
			}	// end F
		}	// end while loop
	}
}
