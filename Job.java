public class Job {
	protected double priority;
	protected int creationTime;
	protected String name;
	// Constructor
	public Job(double p) {
		priority = p;
	}
	public Job (int t, double p) {
		creationTime = t;
		priority = p;
	}
	// Getters and setters
	public double getPriority() {
		return priority;
	}
	public int getCreationTime() {
		return creationTime;
	}
}
