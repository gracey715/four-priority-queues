// Four priority queue implementations:
// Stack implemented as array, jobs pushed to top of stack. FIFO. Priorities ignored.
// Array of four stacks, each implemented as array. Stacks contain jobs within range of priorities.
// Headed linked list. Search for highest priority job.
// Max heap implemented as array. 

// Stack implemented as array
class MyStack extends MyQueue {
	private Job[] stack;
	private int numElements;
	
	public int getNumElements() {
		return numElements;
	}
	public MyStack(int s) {
		stack = new Job[s];
	}
	public boolean empty() {
		return numElements == 0;
	}
	public void add(Job x) {
		stack[numElements] = x;
		numElements++;
	}
	// Returns the job at the top of the stack
	public Job pop() {
		numElements--;
		return stack[numElements];
	}

}	// end Stack

// Array of four stacks
class FourStacks extends MyQueue {
	MyStack[] stack = new MyStack[4];

	// Counters for number of elements of each stack
	int s1count = 0;
	int s2count = 0;
	int s3count = 0;
	int s4count = 0;
	
	public FourStacks(int s) {
		MyStack stack1 = new MyStack(s);		// 0.0 <= priority <= 0.25
		MyStack stack2 = new MyStack(s);		// 0.25 < priority <= 0.5
		MyStack stack3 = new MyStack(s);		// 0.5 < priority <= 0.75
		MyStack stack4 = new MyStack(s);		// 0.75 < priority <= 1.0
		stack[0] = stack1; 
		stack[1] = stack2;
		stack[2] = stack3;
		stack[3] = stack4;
	}

	public int getNumElements() {
		return s1count + s2count + s3count + s4count;
	}
	public boolean empty() {
		return (s1count == 0 && s2count == 0 && s3count == 0 && s4count == 0);
	}
	public void add(Job j) {
		if (j.priority >= 0.0 && j.priority <= 0.25) {
			stack[0].add(j);
			s1count++;
		}
		else if (j.priority > 0.25 && j.priority <= 0.5) {
			stack[1].add(j);
			s2count++;
		}
		else if (j.priority > 0.5 && j.priority <= 0.75) {
			stack[2].add(j);
			s3count++;
		}
		else {
			stack[3].add(j);
			s4count++;
		}
	}
	public Job pop() {
		if (s4count != 0) {
			s4count--;
			return stack[3].pop();
		}
		else if (s3count != 0) {
			s3count--;
			return stack[2].pop();
		}
		else if (s2count != 0) {
			s2count--;
			return stack[1].pop();
		}
		else if (s1count != 0) {
			s1count--;
			return stack[0].pop();
		}
		return null;
	}
}	// end ArrayStacks

// Headed linked list
class MyLinkedList extends MyQueue {
	private MyLinkedList next;
	private static MyLinkedList head;
	private Job job;
	private int numElements;

	public MyLinkedList() {
		next = null;
		numElements = 0;
	}
	public int getNumElements() {
		return numElements;
	}
	public boolean empty() {
		return this.next == null;
	}
	public void add(Job j) {
		MyLinkedList newNode = new MyLinkedList();
		newNode.job = j;
		newNode.next = head;
		head = newNode;
		numElements++;
	}
	// Returns the job with the highest priority
	public Job pop() {
		MyLinkedList current = head.next;
		MyLinkedList highestPrev = head;
		if (current == null) {
			head = head.next;
			numElements--;
			return highestPrev.job;
		}
		double highestPriority = current.job.priority;
		while (current.next != null) {
			if (current.next.job.priority > highestPriority) {
				highestPrev = current;
				highestPriority = highestPrev.next.job.priority;
			}
			current = current.next;
		}
		Job highest = highestPrev.next.job;
		if (highest.priority < head.job.priority) {
			highest = head.job;
			head = head.next;
		}
		else {
			highestPrev.next = highestPrev.next.next;
		}
		numElements--;
		return highest;	
	}
}	// end LL

// Max heap as array
class Heap extends MyQueue {
	private Job[] H;		// the heap
	int count;
	
	public Heap(int s) {
		H = new Job[s];
	}
	public boolean empty() {
		return count == 0;
	}
	public void add(Job x) {
		H[count] = x;
		int q = count;
		count++;
		while (q > 0 && H[(q-1)/2].priority < H[q].priority) {
			Job tmp = H[q];
			H[q] = H[(q-1)/2];
			H[(q-1)/2] = tmp;
			q = (q-1)/2;
		}
	}
	public Job pop() {
		int larger;
		Job m = H[0];
		count--;
		H[0] = H[count];
		int q = 0;
		while (2*q+1 < count) {
			int c1 = 2*q+1;
			int c2 = 2*q+2;
			if (c2 == count)
				larger = c1;
			else if (H[c1].priority > H[c2].priority)
				larger = c1;
			else
				larger = c2;
			if (H[larger].priority < H[q].priority)
				break;
			Job tmp = H[q];
			H[q] = H[larger];
			H[larger] = tmp;
			q = larger;
		}
		return m;
	}

}	// end Heap


