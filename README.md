# Four Priority Queues #

This project contains four implementations of a priority queue: stack, array of four stacks, linked list, and max heap. 
In addition to the four versions, there are two drivers that can be used to test and compare the qualities and running
times of the implementations. The drivers simulate adding jobs of randomized priority (between 0.0 and 1.0) into 
the queue, then executing them.

## The implementations ##

### Stack implemented as array ###
New jobs are placed on the top of the stack, regardless of priority. Job at top of stack is executed first.
This has fast run time but poor quality, as priority is ignored.

### Array of four stacks ###
Each stack is implemented as an array. First stack has jobs 0.0 <= priority <= 0.25. Second stack has jobs 
0.25 < priority <= 0.5. Third stack has jobs 0.5 < priority <= 0.75. Four stack has jobs 0.75 < priority <= 1.0.
Jobs are put on the top of corresponding stack and taken off the top of corresponding stack for execution.

### Linked list ###
A headed linked list. New jobs are added to the front. Search through the linked list to find the highest priority job
and execute. Very slow run time but good quality.

### Max heap ###
Max heap implemented as array. Slow run time but good quality.

## Quality ##

Quality of a queue in this context is the average of the weighted waiting times of each job. 
Weighted waiting time of each job is calculated by the "time" (counter in program) it sits in the queue multiplied 
by its priority.
